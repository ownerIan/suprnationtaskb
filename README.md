
Some information on the application:

- The Online Casino is developed mainly in HTML and Java Script
- Node Js was used to compile code and for the app packaging

- BitBucket and SourceTree were used for the source control of the development. One can follow all the commits entered.
- Project url: https://ownerIan@bitbucket.org/ownerIan/suprnationtaskb.git
- The project contains all the files mentioned in the below

How to install The Online Casino:

- retrieve installation file (.exe), "\suprnation_production\suprnationtaskb\online_casino\dist"
- install file on a windows PC

Please refer to file "assumptions.md" for more details
