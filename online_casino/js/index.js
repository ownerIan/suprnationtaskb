var sumGames = 0;
var gameID = [];

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {

  // get the modal
  var modal = document.getElementById('modal_edit');

  if (event.target == modal) {
    modal.style.display = "none";
  }
}

// Get and update list for the online Casino
function updateList(categoryName) {

  getList(categoryName);

}
// /updateList

// Retrieve list from API
function getList(categoryName) {

  // create dynamic input for search
  var categories_search = document.createElement("input");
  // set input style and function
  categories_search.setAttribute("id", "search_input");
  categories_search.setAttribute("type", "text");
  categories_search.setAttribute("onkeyup", "searchtable();");
  categories_search.setAttribute("placeholder", "Search for data...");
  categories_search.setAttribute("title", "Type in your search");

  var categoriesTopTable = document.createElement("table"); // table
  categoriesTopTable.setAttribute("id", "categories_toptable");

  var categoriesTable = document.createElement("table"); // table
  categoriesTable.setAttribute("id", "categories_datatable");

  var divCategories_bottom = document.createElement("div"); // div
  divCategories_bottom.setAttribute("id", "bottomData");

  // fetch categories plus game ids
  fetch("https://api-live.voodoodreams.com/v1/games/categories")
    .then(response => response.json())
    .then(data => {

      // if no category selected then
      if (categoryName == "") {

        var categoriesTR = categoriesTopTable.insertRow(-1); // table row

        // header 1
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "#";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(0, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 2
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Category";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(1, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 3
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Total Games";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(2, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 4
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Slug";
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(3, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // add JSON data to the table as rows
        for (var i = 0; i < data.categories.length; i++) {

          var categoriesTR = categoriesTable.insertRow(-1); // table row
          categoriesTR.setAttribute("onclick",
            "getList('" + data.categories[i].name + "');");
          categoriesTR.setAttribute("title", "Explore Category");

          // cell 1
          var categoriesTD = document.createElement("td"); // table cell
          categoriesTD.innerHTML = i + 1;
          categoriesTD.setAttribute("class", "dtable_border");
          categoriesTR.appendChild(categoriesTD);

          // cell 2
          var categoriesTD = document.createElement("td"); // table cell
          categoriesTD.innerHTML = data.categories[i].name;
          categoriesTD.setAttribute("class", "dtable_border");
          categoriesTR.appendChild(categoriesTD);

          // cell 3
          var categoriesTD = document.createElement("td"); // table cell
          categoriesTD.innerHTML = data.categories[i]["game-ids"].length;
          // sum Games
          sumGames = sumGames + data.categories[i]["game-ids"].length;
          categoriesTD.setAttribute("class", "dtable_border");
          categoriesTR.appendChild(categoriesTD);

          // cell 4
          var categoriesTD = document.createElement("td"); // table cell
          categoriesTD.innerHTML = data.categories[i].slug;
          categoriesTR.appendChild(categoriesTD);

        }
        // /add JSON data to the table as rows

        divCategories_bottom.innerHTML =
          "[ Count: " + data.categories.length + " ]"
          + "[ Total GAMES: " + sumGames + " ]";

      }
      // else if category selected then
      else {

        var categories_title = document.createElement("h2");
        categories_title.innerHTML = "Category: " + categoryName;

        var divTitle = document.getElementById("dynamich2");
        divTitle.innerHTML = "";
        divTitle.appendChild(categories_title);

        var categoriesTR = categoriesTopTable.insertRow(-1); // table row

        // header 1
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Game Id";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(1, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 2
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Title";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(1, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 3
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Slug";
        categoriesTH.setAttribute("class", "dtable_border");
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(1, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // header 4
        var categoriesTH = document.createElement("th"); // table header
        categoriesTH.innerHTML = "Cover";
        // enable the sort on header click functionality
        categoriesTH.setAttribute("onclick", "sortTable(1, 'asc');");
        categoriesTH.setAttribute("title", "Sort table");
        categoriesTR.appendChild(categoriesTH);

        // add JSON data to the table as rows
        for (var i = 0; i < data.categories.length; i++) {

          if (data.categories[i].name == categoryName) {

            sumGames = data.categories[i]["game-ids"].length;

            // get the list of games
            for (var j = 0; j < data.categories[i]["game-ids"].length; j++) {

              gameID.push(data.categories[i]["game-ids"][j]);

            }
            // /get the list of games

            // fetch categories plus game ids
            fetch("https://api-live.voodoodreams.com/v1/games")
              .then(gresponse => gresponse.json())
              .then(gdata => {

                for (j in gameID) {

                  var categoriesTR = categoriesTable.insertRow(-1); // table row
                  categoriesTR.setAttribute("onclick",
                    "gotoGame('" + gdata.games[gameID[j]].slug + "');");
                  categoriesTR.setAttribute("title", "Go to Game");

                  // cell 1
                  var categoriesTD = document.createElement("td"); // table cell
                  categoriesTD.innerHTML = gameID[j];
                  categoriesTD.setAttribute("class", "dtable_border");
                  categoriesTR.appendChild(categoriesTD);

                  // cell 2
                  var categoriesTD = document.createElement("td"); // table cell
                  categoriesTD.innerHTML = gdata.games[gameID[j]].title;
                  categoriesTD.setAttribute("class", "dtable_border");
                  categoriesTR.appendChild(categoriesTD);

                  // cell 3
                  var categoriesTD = document.createElement("td"); // table cell
                  categoriesTD.innerHTML = gdata.games[gameID[j]].slug;
                  categoriesTD.setAttribute("class", "dtable_border");
                  categoriesTR.appendChild(categoriesTD);

                  // cell 4
                  var categoriesIMG = document.createElement("img"); // table cell image
                  categoriesIMG.setAttribute("src", gdata.games[gameID[j]]["game-image"].src);
                  categoriesIMG.setAttribute("class", "gameCover");
                  var categoriesTD = document.createElement("td"); // table cell
                  categoriesTD.appendChild(categoriesIMG);
                  categoriesTR.appendChild(categoriesTD);

                }

              }
            );
            // fetch game details

          }

        }
        // /add JSON data to the table as rows

        divCategories_bottom.innerHTML =
          "[ Count: " + sumGames + " ]";

        document.getElementById("goBack").style.display = "inline";

      }

    }
  );
  // /fetch categories

  var divContainer_main = document.getElementById("dynamicGamesList");
  divContainer_main.innerHTML = "";

  var divContainer_search = document.createElement("div");
  divContainer_search.setAttribute("id", "search_records");
  divContainer_search.appendChild(categories_search);

  var divCategories_top = document.createElement("div");
  divCategories_top.setAttribute("id", "topData");
  divCategories_top.appendChild(categoriesTopTable);

  var divCategories_table = document.createElement("div");
  divCategories_table.setAttribute("id", "mainData");
  divCategories_table.appendChild(categoriesTable);

  divContainer_main.appendChild(divContainer_search);
  divContainer_main.appendChild(divCategories_top);
  divContainer_main.appendChild(divCategories_table);
  divContainer_main.appendChild(divCategories_bottom);

}
// /getList

// Sort table header functionality
function sortTable(n, dir) {
  var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;

  table = document.getElementById("categories_datatable");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (x < 10) {x = ("0" + x).slice(-2)}
      y = rows[i + 1].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (y < 10) {y = ("0" + y).slice(-2)}
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x > y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x < y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    }
    else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
// /sortTable

// Go through table data and output findings
function searchtable() {

  var input, filter, table, tr, td, i;
  var totalTR = 0;

  input = document.getElementById("search_input");
  filter = input.value.toUpperCase();
  table = document.getElementById("categories_datatable");
  tr = table.getElementsByTagName("tr");
  sumGames = 0;

  for (i = 0; i < tr.length; i++) {
    if (tr[i].innerHTML.toUpperCase().indexOf(">" + filter + "<") > -1) {

      tr[i].style.display = "";

      // if table is not part of category details then
      if (
        document.getElementsByTagName("h2")[0].outerText.slice(0, 9) != "Category:"
      )
      {
        // sum Games
        sumGames = sumGames + Number(tr[i].getElementsByTagName("td")[2].innerHTML);
      }

      // increment total Rows
      totalTR = totalTR + 1;

    } else {
      tr[i].style.display = "none";
    }
  }

  // if table is not part of category details then
  if (
    document.getElementsByTagName("h2")[0].outerText.slice(0, 9) != "Category:"
  )
  {
    document.getElementById("bottomData").innerHTML =
      "[ Count: " + totalTR + " ]"
      + "[ Total GAMES: " + sumGames + " ]";
  }
  else {
    document.getElementById("bottomData").innerHTML =
      "[ Count: " + totalTR + " ]";
  }

}
// /searchtable

// Open up game and details
function gotoGame(slugName) {

  document.getElementById("modal_edit").style.display="block";

  // fetch game
  fetch(
    "https://api-live.voodoodreams.com/v1/games/" + slugName,
    {headers: {"secure": false}}
  )
    .then(response => response.json())
    .then(data => {

      var gameH = document.createElement("h3");
      gameH.innerHTML = data.title;

      var gameIMG = document.getElementById("game_image");
      gameIMG.setAttribute("src", data["game-image"].src);

      var gameLBL = document.getElementById("lblmodal");
      gameLBL.innerHTML = "";
      gameLBL.appendChild(gameH);

      var gameInput = document.getElementById("game_id");
      gameInput.setAttribute("value", data["game-id"]);

      var gameInput = document.getElementById("game_type");
      gameInput.setAttribute("value", data["game-type"]);

      var gameInput = document.getElementById("max_bet");
      gameInput.setAttribute("value", data["maximum-bet"]);

      var gameInput = document.getElementById("max_win");
      gameInput.setAttribute("value", data["maximum-win"]);

      var gameInput = document.getElementById("min_bet");
      gameInput.setAttribute("value", data["minimum-bet"]);

      var gamegame = document.getElementById("game_frame");
      gamegame.setAttribute("src", data.url);
      var divgame = document.getElementById("divGame");
      divgame.setAttribute("onclick",
        "window.open('" + data.url + "', '_blank');");

    }
  );
  // /fetch game


}
// gotoGame
